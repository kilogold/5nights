using UnityEngine;
using System.Collections;

public class CameraSwitch : MonoBehaviour
{
    private Material screenMaterial;
    public Renderer[] SurveilanceMonitors;

    public void SwitchCamera(int cameraId)
    {
        screenMaterial.mainTexture = SurveilanceMonitors[cameraId].GetComponent<Renderer>().material.mainTexture;
    }

    // Use this for initialization
    void Start()
    {
        screenMaterial = GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            SwitchCamera(0);
        }

        if (Input.GetButtonDown("Fire2"))
        {
            SwitchCamera(1);
        }

        if (Input.GetButtonDown("Fire3"))
        {
            SwitchCamera(2);
        }
        
    }
}
